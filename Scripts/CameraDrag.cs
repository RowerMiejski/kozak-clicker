﻿using UnityEngine;

public class CameraDrag : MonoBehaviour
{
    private float mouseStartPos = 0, mouseRelativePos = 0, cameraStartRotation = 0, cameraRelativeRotation = 0;
    private bool isDragable = false, isRotate = true, doRotate = false;
    private float rotation = 0f;
    [SerializeField] GameObject guiObject;
    GUI gui;
    float move;
    float speed = 0f;

    private void Start()
    {
        gui = guiObject.GetComponent<GUI>();
    }
    void FixedUpdate()
    {
        moveCamera();
    }

    void moveCamera()
    {

        if (Input.GetMouseButton(0) && !doRotate && !gui.guiOpen)
        {

            if (isDragable)
            {
                cameraStartRotation = transform.eulerAngles.y;
                mouseStartPos = Input.mousePosition.x;
                isDragable = false;
            }
            mouseRelativePos = Input.mousePosition.x - mouseStartPos;
            cameraRelativeRotation = cameraStartRotation + mouseRelativePos;
            if (cameraRelativeRotation >= 300 && isRotate)
            {
                doRotate = true;
                rotation = -120;
                isRotate = false;
                move = Mathf.Floor((cameraStartRotation + rotation));
            }
            if (cameraRelativeRotation <= -300 && isRotate)
            {
                doRotate = true;
                rotation = 120;

                isRotate = false;
                move = Mathf.Floor((cameraStartRotation + rotation));
            }
        }
        else
        {
            isRotate = true;
            isDragable = true;
            mouseRelativePos = 0;
            mouseStartPos = 0;

            if (doRotate)
            {

                if (move <= -360 || move >= 360)
                {
                    move = 0;
                }
                if (move < 0)
                {
                    move = 360 + move;
                }
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, Mathf.Floor(cameraStartRotation + rotation), 0), 240 * Time.deltaTime);
                float angle = transform.rotation.eulerAngles.y;

                if (Mathf.Floor(angle) == move)
                {
                    doRotate = false;
                    speed = 350f;
                }

            }

        }
    }
}


