﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cart : MonoBehaviour
{
    [SerializeField] float maxResources = 150;
    [SerializeField] TextMesh itemsHeldText;
    [SerializeField] Slider slider;
    [SerializeField] GameObject player;
    [SerializeField] float maxCopacity;
    [SerializeField] float materialAmount;

    Upgrades playerUpgrades;
    Animator animator;
    int island;
    private float itemsHeld;
    bool playing = false;
    public float ridingTime = 1f;
    public float unpackTime = 1f;
    private float timer;
    bool empty = false;
    [SerializeField] GameObject sellObject;
    Bazar bazar;
    void Start()
    {
        animator = GetComponent<Animator>();
        playerUpgrades = player.GetComponent<Upgrades>();
        timer = ridingTime + unpackTime;
        island = playerUpgrades.island;
        bazar = sellObject.GetComponent<Bazar>();
    }

    // Update is called once per frame
    void Update()
    {
        makeOneLap();
    }
    void makeOneLap()
    {
        if (!playing)
        {
            animator.Play("Forward");
            playing = true;
        }


        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Forward"))
        {
            timer -= Time.deltaTime;
            if(timer <= unpackTime)
            {
                if (playerUpgrades.materialAmount == 0)
                {
                    timer = ridingTime + unpackTime;
                    animator.Play("Backward");
                    empty = true;
                }
                slider.value = 1 - timer;

            }
            if (timer <= 0 && !empty)
            {
                if (playerUpgrades.materialAmount > maxCopacity)
                {
                    materialAmount += maxCopacity;
                    playerUpgrades.materialAmount -= maxCopacity;
                }
                else
                {
                    materialAmount += playerUpgrades.materialAmount;
                    playerUpgrades.materialAmount = 0;


                }

                playerUpgrades.updateText();
                itemsHeldText.text = materialAmount.ToString();
                animator.Play("Backward");
                timer = ridingTime + unpackTime;
                slider.value = 0;
            }
            empty = false;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Backward"))
        {
            if (timer <= unpackTime)
            {
                slider.value = 1 - timer;
            }
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                playing = false;
                timer = ridingTime + unpackTime;
                slider.value = 0;
                bazar.addMaterials(materialAmount, island);
                materialAmount = 0;
                itemsHeldText.text = materialAmount.ToString();

            }
        }
    }
    void changePackTime()
    {
        //unpackTime = unpackTime / 2;

    }
}
