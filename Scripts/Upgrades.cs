﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrades : MonoBehaviour
{
    //1 - dirt, 2 - sand, 3 - stone
    [SerializeField] Slider slider;
    [SerializeField] float digTime = 1f;
    [SerializeField] float copacity;
    [SerializeField] float perClick;
    public float materialAmount;

    [SerializeField] TextMesh rock;
    [SerializeField] public int island;
    Animator animator;
    [SerializeField] GameObject camera;
    String perClickSave = "perClick", copacitySave = "copacity", materialAmountSave = "materialAmount", automationSave = "automation";
    private bool indicatorOn = false;
    private bool clicked = false;
    float timer;
    public float rockAmount = 0f;
    bool animationPlaying = false;
    private int automation = 0;
    int savedGame = 0;

    private void Awake()
    {
        formatSaveString();
        loadSave();
        updateText();

    }
    void Start()
    {
        animator = GetComponent<Animator>();
        slider.gameObject.SetActive(false);
        timer = digTime;
    }

    void Update()
    {
        changeSliderValue();
    }

    private void changeSliderValue()
    {
        if ((indicatorOn || automation == 1 )&& materialAmount < copacity)
        {
            if (!animationPlaying)
            {
                animator.Play("digAnim");
                animationPlaying = true;
            }

            timer -= Time.deltaTime;
            slider.value = timer;
            if (timer <= 0)
            {
                slider.gameObject.SetActive(false);

                if(materialAmount + Mathf.Floor(perClick) > copacity)
                {
                    materialAmount = copacity;
                }
                else
                {
                    materialAmount += Mathf.Floor(perClick);
                }
                PlayerPrefs.SetFloat(materialAmountSave, materialAmount);
                rockAmount += materialAmount;
                updateText();
                indicatorOn = false;
                timer = digTime;
                animationPlaying = false;
            }
        }
    }

    public void showClickIndicator()
    {
        if(!indicatorOn)
        {
            slider.value = digTime;
            slider.maxValue = digTime;
            slider.gameObject.SetActive(true);
        }
        indicatorOn = true;

    }
    public void upgradePerClick()
    {
        perClick += 1 + perClick * 0.1f;
        PlayerPrefs.SetFloat(perClickSave, perClick);
        //
    }
    public void upgradeCopacity()
    {
        copacity += Mathf.Floor(0.1f * copacity);
        PlayerPrefs.SetFloat(copacitySave, copacity);
        print(PlayerPrefs.GetFloat(copacitySave));
    }
    public void updateText()
    {
        rock.text = materialAmount.ToString();

    }
    public void automateUpgrade()
    {
        automation = 1;
        PlayerPrefs.SetInt(automationSave, automation);

    }
    void loadSave()
    {
        copacity = PlayerPrefs.GetFloat(copacitySave);
        perClick = PlayerPrefs.GetFloat(perClickSave);
        materialAmount = PlayerPrefs.GetFloat(materialAmountSave);
        automation = PlayerPrefs.GetInt(automationSave);
    }
    void saveGame()
    {
        PlayerPrefs.SetFloat(copacitySave, copacity);
        PlayerPrefs.SetFloat(perClickSave, perClick);
        PlayerPrefs.SetFloat(materialAmountSave, materialAmount);
        PlayerPrefs.SetInt(automationSave, automation);
        savedGame = 1;
        PlayerPrefs.Save();

    }
    void formatSaveString()
    {
        materialAmountSave += island.ToString();
        perClickSave += island.ToString();
        copacitySave += island.ToString();
        automationSave += island.ToString();
    }
    void resetProgress()
    {
        PlayerPrefs.DeleteAll();
        perClick = 1;
        copacity = 100;
        materialAmount = 0;
        automation = 0;
        savedGame = 0;
    }
    void loadDefaultSettings()
    {
        perClick = 1;
        copacity = 100;
        materialAmount = 0;
        automation = 0;
    }
    private void OnApplicationQuit()
    {
        saveGame();
        print("zapisaywane");
    }
}