﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bazar : MonoBehaviour
{
    // 0 - dirt, 1 - sand, 2 - stone
    int[] materialAmount = new int[3];
    float money = 0;
    int[] sellPercent = new int[3];
    int[] amountToSell = new int[3];
    int[] pricePerOne = new int[3];
    [SerializeField] TextMeshProUGUI checkOut;
    [SerializeField] TextMeshProUGUI moneyAmount;
    [SerializeField] TextMeshProUGUI[] amountText;
    [SerializeField] TextMeshProUGUI[] materialAmountToSell;
    [SerializeField] TextMeshProUGUI[] materialSummedPrice;
    [SerializeField] TextMeshProUGUI[] percentUpText;
    [SerializeField] TextMeshProUGUI[] sellPriceText;
    [SerializeField] TextMeshProUGUI[] pricePerText;
    [SerializeField] GameObject guiObject;
    GUI gui;

    public void Awake()
    {
    }
    void Start()
    {
        pricePerOne[0] = 12;
        materialAmount[0] = 100;
        sellPercent[0] = 20;
        updateAllText();
        updateSellPriceText();
        gui  = guiObject.GetComponent<GUI>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //zmienia cene
    void changePrices(int newPrice, int material)
    {
        pricePerOne[material] = newPrice;
    }

    //po wcisnieciu wielkiego przycisku sprzedawania
    public void sellItems()
    {
        int i = 0;
        int cena = 0;
        foreach (int amount in amountToSell)
        {
            cena += amountToSell[i] * pricePerOne[i];
            materialAmount[i] -= amountToSell[i];
            i++;
        }
        gui.changeMoneyValue(cena);
        updateAllText();
    }

    // zmienia pokazana cene przy sprzedawaniu
    void updateBigGreebButtonPriceText()
    {
        money = 0;
        int i = 0;
        foreach (int amount in amountToSell)
        {
            money += (amount * pricePerOne[i]);
            i++;
        }
        checkOut.text = money + "$";
        
    }
    // przy wcisnieciu guzika plus
    public void higherSellAmount(int island)
    {
        if(sellPercent[island] <= 90)
        {
            sellPercent[island] += 10;
        }
        updateSellAmountPerPiece();
    }


    // przy wcisnieciu guzika minus

    public void lowerSellAmount(int island)
    {
        if (sellPercent[island] >= 10)
        {
            sellPercent[island] -= 10;
        }
        updateSellAmountPerPiece();
    }


    // zmienia pokazana ilosc surowcow do sprzedanaia
    void updateSellAmountPerPiece()
    {
        int i = 0;
        foreach (TextMeshProUGUI text in materialAmountToSell)
        {
            amountToSell[i] = Mathf.FloorToInt(materialAmount[i] * sellPercent[i] / 100);
            text.text = amountToSell[i].ToString();
            i++;
        }
        updatePriceSum();
    }


    // ilosc surowcow
    void updateAmountText()
    {
        int i = 0;
        foreach(TextMeshProUGUI text in amountText)
        {
            text.text = materialAmount[i].ToString();
            i++;
        }
    }


    //dodawanie materialow przez wuzek
    public void addMaterials(float material, int island)
    {
        materialAmount[island] += (int) material;
        updateAllText();

    }


     // zmienia zsumowana cene za sztuke
    void updatePriceSum()
    {
        int i = 0;
        foreach (TextMeshProUGUI text in materialSummedPrice)
        {
            text.text = (amountToSell[i] * pricePerOne[i]).ToString() + "$";
            i++;
        }
        updateBigGreebButtonPriceText();
    }


    // price per one
    void updateSellPriceText()
    {
        int i = 0;
        foreach (TextMeshProUGUI text in sellPriceText)
        {
            text.text = pricePerOne[i] + "$";
            i++;
        }
    }
    void updateAllText()
    {
        updateSellPriceText();
        updateSellAmountPerPiece();
        updateAmountText();
    }
}
