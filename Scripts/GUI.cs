﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUI : MonoBehaviour
{
    [SerializeField] GameObject popupPanels;
    GameObject lastGuiItem;
    float money = 0;
    [SerializeField] GameObject player;
    [SerializeField] GameObject camera;
    [SerializeField] GameObject upgradeMenu;
    [SerializeField] GameObject[] panels;
    [SerializeField] TextMeshProUGUI moneyText;
    ScrollRect scrollRect;
    bool isChangeNeeded = true;
    public bool guiOpen = false;
    int currentIsland; // 1 - dirt, 2 - sand, 3 - stone

    public void Awake()
    {
        closeAllPanels();
        scrollRect = upgradeMenu.GetComponent<ScrollRect>();
    }

    public void activatePanel(GameObject panel)
    {
        if (isChangeNeeded)
        {
            changeCurrentUpgrade(getCurrentIsland());
        }
        panel.SetActive(!panel.activeSelf);
        guiOpen = !guiOpen;
        if(!(lastGuiItem is null) && lastGuiItem != panel)
        {
            lastGuiItem.SetActive(false);
        }
        lastGuiItem = panel;
        isChangeNeeded = true;
    }
    public void closeAllPanels()
    {
        for (int j = 0; j < popupPanels.transform.childCount; j++)
        {
            popupPanels.transform.GetChild(j).gameObject.SetActive(false);
        }
        guiOpen = false;
    }
    int getCurrentIsland()
    {
        if (camera.transform.eulerAngles.y > 230 && camera.transform.eulerAngles.y < 250)
        {
            currentIsland = 2;
            return 3;

        }
        else if (camera.transform.eulerAngles.y > 110 && camera.transform.eulerAngles.y < 130)
        {
            currentIsland = 1;
            return 1;

        }
        else if (camera.transform.eulerAngles.y >= 0 && camera.transform.eulerAngles.y < 10)
        {
            currentIsland = 3;
            return 1;

        }
        return 1;
    }   
    public void changeCurrentUpgrade(int island)
    {
        currentIsland = island;
        closeAllIslandMenus();
        scrollRect.content = panels[island -1].GetComponent<RectTransform>();
        panels[island - 1].SetActive(true);
        isChangeNeeded = false;
    }   
    void closeAllIslandMenus()
    {
        foreach (GameObject panel in panels) {
            panel.SetActive(false);
        }
    }
    public void changeMoneyValue(int amount)
    {
        money += amount;
        updateMoneyText();
    }
    void updateMoneyText()
    {
        moneyText.text = money + "$";
    }
}
